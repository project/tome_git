<?php

namespace Drupal\tome_git\EventSubscriber;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\State\StateInterface;
use Drupal\tome_git\Form\TomeGitForm;
use Drupal\tome_sync\Event\TomeSyncEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Flags that a sync as being needed if anything is exported by Tome Sync.
 */
class ExportEventSubscriber implements EventSubscriberInterface {

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * ExportEventSubscriber constructor.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * Updates the state to indicate that a sync is needed.
   */
  public function updateState() {
    $this->state->set(TomeGitForm::STATE_KEY_NEEDS_SYNC, TRUE);
    Cache::invalidateTags(['tome_git:needs_sync']);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[TomeSyncEvents::EXPORT_CONTENT][] = ['updateState'];
    $events[TomeSyncEvents::DELETE_CONTENT][] = ['updateState'];
    $events[ConfigEvents::SAVE][] = ['updateState'];
    $events[ConfigEvents::DELETE][] = ['updateState'];
    $events[ConfigEvents::RENAME][] = ['updateState'];
    return $events;
  }

}
