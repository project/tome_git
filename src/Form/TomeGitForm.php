<?php

namespace Drupal\tome_git\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Process\Exception\ProcessTimedOutException;
use Symfony\Component\Process\Process;
use Drupal\Core\State\StateInterface;

/**
 * Contains a basic form that syncs Tome directories using Git.
 */
class TomeGitForm extends FormBase {

  /**
   * The key user interfaces should use to check if a sync is needed.
   */
  const STATE_KEY_NEEDS_SYNC = 'tome_git.needs_sync';

  /**
   * The content storage.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $contentStorage;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * TomeGitForm constructor.
   *
   * @param \Drupal\Core\Config\StorageInterface $content_storage
   *   The content storage.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   */
  public function __construct(StorageInterface $content_storage, StateInterface $state) {
    $this->contentStorage = $content_storage;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tome_sync.storage.content'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tome_git_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $git_directory = Settings::get('tome_git_base_directory', '../');
    $result = $this->runCommand('git status .', $git_directory);
    if ($result === FALSE) {
      $this->messenger()->addError($this->t('Unable to check status - this likely means Git is not functioning or the "tome_git_base_directory" setting has not been properly configured'));
    }
    $form['description'] = [
      '#markup' =>
      '<p>' . $this->t('Submitting this form will perform Git operations to keep your local Tome files in sync with your remote files.') . '</p>'
      . '<p>' . $this->t('This is most useful when pushing changes to Tome files, if you are pulling changes to content or config you will need to run a Tome import using the command line after syncing changes.') . '</p>',
    ];
    $form['status'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Status'),
      'text' => [
        '#markup' => '<pre>' . Html::escape($result) . '</pre>',
      ],
    ];
    $form['auto_resolve'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto resolve merge conflicts'),
      '#description' => $this->t('If the pull and rebase fails, automatically resolve conflicts by preferring your changes. <strong>Note that this could result in changes from another editor being lost.</strong>'),
      '#default_value' => FALSE,
    ];
    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Commit message'),
      '#description' => 'The commit message if there are any changes.',
      '#default_value' => $this->t('Synced Tome changes using the UI.'),
      '#rows' => 2,
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sync changes'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $git_directory = Settings::get('tome_git_base_directory', '../');
    $ssh_agent = $this->runCommand('ssh-agent -s', $git_directory);
    if (!$ssh_agent) {
      $this->messenger()->addError($this->t('Unable to start ssh-agent'));
      return;
    }
    $this->gitSync($ssh_agent, $form_state->getValue('message'), $form_state->getValue('auto_resolve'));
    $this->runCommand("$ssh_agent ssh-agent -k", $git_directory);
  }

  /**
   * Syncs changes to Tome directories using Git.
   *
   * @param string $ssh_agent
   *   The SSH agent string.
   * @param string $message
   *   The commit message, if there are any changes.
   * @param bool $auto_resolve
   *   Whether or not conflicts should be automatically resolved.
   *
   * @return bool
   *   Whether or not the sync succeeded.
   */
  protected function gitSync($ssh_agent, $message, $auto_resolve = FALSE) {
    $git_directory = Settings::get('tome_git_base_directory', '../');
    $ssh_key = Settings::get('tome_git_ssh_key', '~/.ssh/id_rsa');
    if ($this->runCommand("$ssh_agent ssh-add $ssh_key", $git_directory) === FALSE) {
      $this->messenger()->addError($this->t('Unable to add SSH key'));
      return FALSE;
    }
    // Do an initial fetch, which mostly just validates repository access.
    if ($this->runCommand("$ssh_agent git fetch", $git_directory) === FALSE) {
      $this->messenger()->addError($this->t('Unable to perform initial fetch'));
      return FALSE;
    }
    // Add changes if any are present.
    $result = $this->runCommand('git status --porcelain .', $git_directory);
    if ($result === FALSE) {
      $this->messenger()->addError($this->t('Unable to check status'));
      return FALSE;
    }
    $has_changes = !empty($result);
    if ($this->runCommand('git add .', $git_directory) === FALSE) {
      $this->messenger()->addError($this->t('Unable to add changes in @directory'));
      return FALSE;
    }
    // Commit changes.
    if ($has_changes) {
      if ($this->runCommand('git commit -m ' . escapeshellarg($message), $git_directory) === FALSE) {
        $this->messenger()->addError($this->t('Unable to sync with Git'));
        return FALSE;
      }
    }
    // Pull and rebase, aborting on error.
    if ($this->runCommand("$ssh_agent git pull --rebase", $git_directory, FALSE) === FALSE) {
      if ($auto_resolve) {
        $this->gitAutoResolve();
      }
      if ($this->runCommand("git rebase --continue", $git_directory) === FALSE) {
        $this->runCommand('git rebase --abort', $git_directory, FALSE);
        $this->messenger()->addError($this->t('Unable to pull changes before push. You may have to resolve merge or rebase conflicts manually.'));
        return FALSE;
      }
    }
    // Push changes.
    if ($this->runCommand("$ssh_agent git push", $git_directory) === FALSE) {
      $this->t('Unable to push changes.');
      return FALSE;
    }
    $this->messenger()->addStatus($this->t('Changes synced with Git!'));

    $url = Url::fromRoute('tome_sync.import_partial');
    if ($url->access()) {
      $this->messenger()->addStatus($this->t('To import content or config changes, visit the <a href=":url">Tome Sync form</a>.', [
        ':url' => $url->toString(),
      ]));
    }

    $this->state->set(self::STATE_KEY_NEEDS_SYNC, FALSE);
    Cache::invalidateTags(['tome_git:needs_sync']);
    return TRUE;
  }

  /**
   * Attempts to auto-resolve Git conflicts.
   *
   * Conflicts are resolved by doing a JSON merge for the content index file,
   * and preserving local changes for all other conflicts.
   */
  protected function gitAutoResolve() {
    $git_directory = Settings::get('tome_git_base_directory', '../');
    $result = $this->runCommand('git --no-pager diff --name-only --diff-filter=U', $git_directory);
    if (!$result) {
      return;
    }
    // Resolve merge conflicts in index file, which are common.
    if (preg_match_all('|(.*meta/index.json)|', $result, $matches)) {
      $index_file = $matches[1][0];
      // Merge the index from "ours" into "theirs". During a rebase "theirs"
      // is the branch being rebased (the current branch), which is the
      // opposite of a merge.
      $ours_index = json_decode($this->runCommand("git show :2:$index_file", $git_directory), TRUE);
      $theirs_index = json_decode($this->runCommand("git show :3:$index_file", $git_directory), TRUE);
      $index_file_absolute = realpath($git_directory . '/' . $index_file);
      if (file_exists($index_file_absolute) && $ours_index && $theirs_index) {
        $content_list = $this->contentStorage->listAll();
        foreach ($ours_index as $name => $dependencies) {
          if (isset($theirs_index[$name]) || (!isset($theirs_index[$name]) && isset($content_list[$name]))) {
            foreach ($dependencies as $dependency) {
              if (isset($content_list[$dependency])) {
                $theirs_index[$name][] = $dependency;
              }
            }
            $theirs_index[$name] = array_unique($theirs_index[$name]);
          }
        }
        $final_index = json_encode($theirs_index, JSON_PRETTY_PRINT);
        file_put_contents($index_file_absolute, $final_index);
        $this->runCommand("git add $index_file", $git_directory);
      }
    }
    // Prefer the local changes for all other files.
    if ($result = $this->runCommand('git --no-pager diff --name-only --diff-filter=U', $git_directory)) {
      $this->runCommand('git checkout --theirs -- ' . str_replace("\n", ' ', $result), $git_directory);
      $this->runCommand('git add .', $git_directory);
    }
  }

  /**
   * Run a command and get its result.
   *
   * @param string $command
   *   The command.
   * @param string $cwd
   *   The current working directory.
   * @param bool $show_error
   *   Whether or not errors should be shown. Defaults to TRUE.
   *
   * @return bool|string
   *   Returns FALSE on error, and the process output on success.
   */
  protected function runCommand($command, $cwd, $show_error = TRUE) {
    $process = new Process($command, $cwd, [
      'GIT_SSH_COMMAND' => 'ssh -oBatchMode=yes',
      'PATH' => Settings::get('tome_git_env_path', '/usr/local/bin:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin'),
    ]);
    $process->setTimeout(10);
    try {
      $process->run();
    }
    catch (ProcessTimedOutException $e) {
      $this->messenger()->addError('Error: Process timed out, it\'s possible SSH or Git prompted for input');
      return FALSE;
    }
    if (!$process->isSuccessful()) {
      if ($show_error && $error_output = $process->getErrorOutput()) {
        $this->messenger()->addError($this->t('Error: @error', [
          '@error' => $error_output,
        ]));
      }
      return FALSE;
    }
    return $process->getOutput();
  }

}
