# tome_git

Tome Git is a Drupal module that provides a simple form for syncing your local
content, config, and file changes with Git.

# Installation and use

1. Install the tome_git module
2. Set the `tome_git_base_directory` setting in settings.php to a base
directory to run all Git commands. This defaults to `../`, which on a Composer
site would probably be your root repository directory.
3. Set the `tome_git_ssh_key` setting in settings.php to the path of the
private SSH key Tome Git should use for Git operations. The SSH key must not be
password protected. This defaults to `~/.ssh/id_rsa`.
4. Set the `tome_git_env_path` setting to a `$PATH`-style string that contains
paths to common commands and specifically `git`, `ssh-add`, and `ssh-agent`.
This defaults to
`/usr/local/bin:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin`.

# Notes

The private SSH key for Tome Git should be specific to the site repository and,
if possible, the site environment. If you're using continuous delivery it may
be wise to set the private SSH key as an environmental variable, and run
something like:

```
echo "$TOME_GIT_SSH_KEY" > ~/.ssh/tome_git_rsa
echo '$settings["tome_git_ssh_key"] = "~/.ssh/tome_git_rsa";' >> web/sites/default/settings.env.php'
```

But this is just one example of how you can automate key deployment.

Note that the SSH key does not use password protection as adding the key to an
SSH agent programmatically in Tome Git would require the password to be present
in a settings.php setting, environmental variable, or somewhere else that is
also readable by the web user. Restricting what the key can access and how it
is deployed and stored long term are better paths to security. 
